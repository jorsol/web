+++
title = "About me: Jorge Solórzano"
+++

I am a Java developer, Postgres advocate and Kubernetes enthusiast from Nicaragua,
working as a Senior Software Engineer at <a href="https:/www.ongres.com" target="_blank">OnGres</a>,
now living in Madrid, Spain.

I'm a passionate open source advocate and developer with several contributions to projects like
[PgJDBC](https://github.com/pgjdbc/pgjdbc), [Quarkus](https://github.com/quarkusio/quarkus),
[Fabric8 Kubernetes Client](https://github.com/fabric8io/kubernetes-client).

Currently working on [StackGres](https://stackgres.io) a cloud-native PostgreSQL operator for Kubernetes,
and [postgresqlCO.NF](https://postgresqlco.nf) a docummentation and configuration manager for PostgreSQL
parameters.

My volunteer experience goes from being a core member, co-organizer, and speaker in conferences and
meetups of open source communities like [PostgreSQL España](https://www.meetup.com/PostgreSQL-Espana/),
[Java Nicaragua](https://javanicaragua.org) and Ubuntu Nicaragua.