# Weblog based on Hugo

This is my personal weblog based on Hugo using the Mediumish Theme.

## License

The content of this site is licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).

The hugo Mediumish Theme is licensed under the [MIT License](https://choosealicense.com/licenses/mit/).